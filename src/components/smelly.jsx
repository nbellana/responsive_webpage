import React from "react";
import "../css/page.css";

/**
 * @returns smiley section
 */
const smelly = () => {
  return (
    <div>
      <section id="smelly">
        <h2>: )</h2>
      </section>
    </div>
  );
};

//Exporting smelly globally as a component
export default smelly;
