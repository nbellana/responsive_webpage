import React from "react";
import "../css/page.css";

/**
 *
 * @param {*} props properties of the threecol object in app.js are passed down to col3 function
 * @returns Three column section
 */
const col3 = (props) => {
  return (
    <div>
      <section className="inner-wrapper-3">
        {props.threecol.map((data) => {
          return (
            <section className="one-third" id={data.id} key={data.id}>
              <h3>{data.h3}</h3>
              <p>{data.description}</p>
            </section>
          );
        })}
      </section>
    </div>
  );
};
//col3 function exposed globally as a component
export default col3;
