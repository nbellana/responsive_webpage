import React from "react";
import "../css/page.css";

const banner = () => {
  return (
    <div>
      <section className="banner">
        <div className="banner-inner">
          <img
            src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571172/rocket_design_k4nzbm.png"
            alt="rocket_design"
          />
        </div>
      </section>
    </div>
  );
};

//Exporting banner globally as a component
export default banner;
