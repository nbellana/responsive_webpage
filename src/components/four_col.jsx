import React from "react";
import "../css/page.css";

/**
 *
 * @param {*} props  properties of the fourcol_icons object in app.js are passed down to Fourcols function
 * @returns Four column section
 */
const Fourcols = (props) => {
  return (
    <div>
      {props.fourcol_icons.map((data) => {
        return (
          <section className="one-fourth" id={data.id} key={data.id}>
            <i className={data.icon}></i>

            <h3>{data.h3}</h3>
          </section>
        );
      })}
    </div>
  );
};

//Exporting Fourcols globally as a component
export default Fourcols;
