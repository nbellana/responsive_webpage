import React from "react";
import "../css/page.css";

/**
 *
 * @param {*} props property values given in <Wrap/> component in Wrapper function are passed down to Wrap function
 * @returns returns Wrap section
 */
const Wrap = (props) => {
  if (props.class === "inner-wrapper") {
    return (
      <section className={props.class}>
        <article id={props.article_id}>
          <img src={props.src} alt={props.alt} />
        </article>
        <aside id={props.aside_id}>
          <h2>{props.h2}</h2>
          <p>{props.description}</p>
        </aside>
      </section>
    );
  } else if (props.class === "inner-wrapper-2") {
    return (
      <section className={props.class}>
        <article id={props.article_id}>
          <h2>{props.h2}</h2>
          <p>{props.description}</p>
        </article>
        <aside className={props.aside_id}>
          <img src={props.src} alt={props.alt} />
        </aside>
      </section>
    );
  }
};

const Wrapper = () => {
  return (
    <div>
      <Wrap
        class="inner-wrapper"
        src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png"
        alt="hand_ipad"
        aside_id="tablet2"
        h2="Mobile.Tablet.Desktop."
        description="A clean HTML layout and CSS stylesheet making for a great responsive
            framework to design around that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin.jQuery plugin.that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin."
      />

      <Wrap
        class="inner-wrapper-2"
        article_id="mobile"
        h2="Accross Each Device"
        description="A clean HTML layout and CSS stylesheet making for a great responsive
            framework to design around that includes a responsive drop down
            navigation menu, image slider, contact form and ‘scroll to the top’
            jQuery plugin.that includes a responsive drop down navigation menu,
            image slider, contact form and ‘scroll to the top’ jQuery plugin."
        aside_id="hand-mobile"
        src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png"
        alt="hand_mobile"
      />

      <Wrap
        class="inner-wrapper"
        src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png"
        alt="desktop"
        aside_id="desktop"
        h2="Desktop"
        description="Also included with the Template is the Template Customization Guide
            with five special video lessons showing you how to get professional
            website pictures & how to edit them to fit the template, how to make
            a custom website logo, how to upload your HTML website template to
            the internet and an HTML website building tools video!"
      />
    </div>
  );
};

export default Wrapper;
