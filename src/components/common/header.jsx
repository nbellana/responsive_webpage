import React from "react";
import "../../css/page.css";

const header = (props) => {
  return (
    <div>
      <div id="header-inner">
        <a href="/" id="logo">
          {" "}
        </a>
        <nav>
          <a href="/" id="menu-icon">
            {" "}
          </a>
          <ul>
            {props.navlinks.map((data) => {
              return (
                <li key={data.navlinks}>
                  <a href="/">{data.navlinks}</a>
                </li>
              );
            })}
          </ul>
        </nav>
      </div>
    </div>
  );
};

//Exporting header globally as a component
export default header;
