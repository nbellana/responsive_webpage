import React from "react";
import "../../css/page.css";

/**
 *
 * @param {*} props properties of the icon object in app.js are passed down to Footer component
 * @returns First footer section
 */
const Footer = (props) => {
  return (
    <div>
      <footer>
        <ul className="social">
          {/* Maps the properties of icons object in app.js to li element */}
          {props.icons.map((data) => {
            return (
              <li key={data.class}>
                <a href="/" target="_blank">
                  <i className={data.class}></i>
                </a>
              </li>
            );
          })}
        </ul>
      </footer>
    </div>
  );
};

//Exporting Footer globally as a component
export default Footer;
