import React from "react";
import "../../css/page.css";

/**
 * Represents the second footer of the website
 */
const footer2 = () => {
  return (
    <div>
      <footer className="second">
        <p>&copy; MaxPower Design</p>
      </footer>
    </div>
  );
};

//Exporting footer2 globally as a component
export default footer2;
