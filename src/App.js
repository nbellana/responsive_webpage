import React from "react";
import "./css/page.css";

/**
 * Header and footer are common componenets for the website even though we navigate through different pages in it.
 */
import Header from "./components/common/header";
import Footer from "./components/common/footer";
import Footer2 from "./components/common/footer2";

/**
 * These are the dependent components having dynamic content to render.
 */
import Banner from "./components/banner";
import Columns from "./components/four_col";
import Threecol from "./components/three_col";
import Wrapper from "./components/wrapper";
import Smelly from "./components/smelly";

/**
 * navlinks is the data object for navigation links. This data is passed down to <Header/> component.
 */
let navlinks = [
  {
    navlinks: "Skills",
  },
  {
    navlinks: "Portfolio",
  },
  {
    navlinks: "Our team",
  },
  {
    navlinks: "Contact",
  },
];

/**
 * icons is the data object for social icons in footer. This data is passed down to <Footer/> component.
 * It has single property called class with fontawesome classes as values.
 */
let icons = [
  {
    class: "fa fa-facebook",
  },
  {
    class: "fa fa-google-plus",
  },
  {
    class: "fa fa-twitter",
  },
  {
    class: "fa fa-youtube",
  },
  {
    class: "fa fa-instagram",
  },
];

/**
 * fourcol_icons is a data object for four column section. This data is passed down to <Columns/> component.
 * It has three properties id, icon and h3.
 */
let fourcol_icons = [
  {
    id: "html",
    icon: "fa fa-html5",
    h3: "HTML",
  },
  {
    id: "css",
    icon: "fa fa-css3",
    h3: "CSS",
  },
  {
    id: "seo",
    icon: "fa fa-search",
    h3: "SEO",
  },
  {
    id: "social",
    icon: "fa fa-users",
    h3: "Social",
  },
];

/**
 * threecol is a data object for three column secion. We are passing down this data to <Threecol/> component.
 * It has three properties id, h3 and description.
 */
let threecol = [
  {
    id: "google",
    h3: "Google Search",
    description:
      " Also included with the Template is the Template Customization Guide with five special video lessons showing you how to get professional website pictures & how to edit them to fit the template,flessons showing you how to get professional website pictures & how to edit them to fit the template,f",
  },
  {
    id: "marketing",
    h3: "Marketing",
    description:
      "Note: this template includes a page with a PHP website contact form and requires a web host or a program such as XAMPP to run PHP and display it’s content.id='desktop' lessons showing you how to get professional website pictures & how to edit them to fit thetemplate,f",
  },
  {
    id: "customers",
    h3: "Happy Customers",
    description:
      "When you purchase and download The Rocket Design HTML Template you get a full five page responsive HTML website template with both a“light” and “dark” version of the template in addition to the following features:",
  },
];

function App() {
  return (
    <div>
      <Header navlinks={navlinks} />
      <Banner />
      <Columns fourcol_icons={fourcol_icons} />
      <Wrapper />
      <Threecol threecol={threecol} />
      <Smelly />
      <Footer icons={icons} />
      <Footer2 />
    </div>
  );
}

// Exporting app globally as a component
export default App;
